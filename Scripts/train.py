import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix, make_scorer, accuracy_score, precision_score, recall_score, f1_score
from sklearn.model_selection import cross_val_score, cross_validate
from models import RandomForest, Linear_SVC, AdaBoost, GradientBoost, XGBoost
from imblearn.over_sampling import RandomOverSampler

# get cv mean scores
def get_cv_scores(model, X_train, X_test, y_train, y_test):
    scoring = {'accuracy' : make_scorer(accuracy_score), 
                'precision' : make_scorer(precision_score, average='weighted'),
                'recall' : make_scorer(recall_score, average='weighted'), 
                'f1_score' : make_scorer(f1_score, average='weighted')}

    X = np.vstack((X_train, X_test))
    y = np.append(y_train, y_test)
    scores = cross_validate(model, X, y, cv=5, scoring=scoring)
    
    for score_type, vals in scores.items():
        scores[score_type] = np.mean(vals)
        if(score_type == 'test_accuracy'):
            print("Acc Std:", np.std(vals))
    
    print(scores)

# get the scores
def get_scores(model, X_train, X_test, y_train, y_test):
    y_pred = model.predict(X_test) 

    # model accuracy for X_train 
    accuracy = model.score(X_train, y_train) 
    print('train accuracy', accuracy)   

    # model accuracy for X_test   
    accuracy = model.score(X_test, y_test) 
    print('test accuracy', accuracy)   

    # creating a confusion matrix 
    cm = confusion_matrix(y_test, y_pred) 
    print(cm)
         
    # classification report for classwise precision-recall-f1
    print('classification_report')
    print(classification_report(y_test, y_pred))    
   
    # 5-CV
    print('5-fold CV scores') 
    get_cv_scores(model, X_train, X_test, y_train, y_test) 
    print()

def run_sklearn_experiments(X_train, X_test, y_train, y_test):
    
    print('X_train.shape:', X_train.shape)
    print('X_test.shape:', X_test.shape)
    print('y_train.shape:', y_train.shape)
    print('y_test.shape:', y_test.shape)

    ada_boost_model = AdaBoost(X_train, y_train)
    get_scores(ada_boost_model, X_train, X_test, y_train, y_test)
            
    grad_boost_model = GradientBoost(X_train, y_train)
    get_scores(grad_boost_model, X_train, X_test, y_train, y_test)

    linear_svc_model = Linear_SVC(X_train, y_train)
    get_scores(linear_svc_model, X_train, X_test, y_train, y_test)

    random_forest_model = RandomForest(X_train, y_train)
    get_scores(random_forest_model, X_train, X_test, y_train, y_test)